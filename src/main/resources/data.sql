


INSERT INTO authors(name, surname, nickname) VALUES ('Jason', 'Kolikien', 'Magister');
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Il Signore degli agnelli', '23444', 'storia di una pasqua finita male', 'Mondradori', '1765-04-05', 99.00, 0);
INSERT INTO book_author(book_id, author_id) VALUES (1, 1);


INSERT INTO users(username, password) VALUES ('admin', '$2a$10$yxZjf/A5ndtHO7zfZ3HsbuG7AZ2RCCMn75f7FoOO1E2x2/BtJFiK6' );