package com.ictcampus.lab.base.repository.book.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;



@Data
@Table( name = "books" )
@Entity
public class BookEntity {
	@Id
	@GeneratedValue
	private Long id;
	private String title;
	private String isbn;

	@Column( name = "abstract" )
	private String descAbstract;
	private String description;
	private String publisher;

	@Column(name = "published_date")
	private LocalDate publishedDate;
	private BigDecimal price;
	private BigDecimal discount;

	@ManyToMany
	@JoinTable(
			name = "book_author",
			joinColumns = @JoinColumn(name = "book_id"),
			inverseJoinColumns = @JoinColumn(name = "author_id")
	) @ToString.Exclude
	private List<AuthorEntity> authors = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "thumbnail_id")
	private ImageEntity thumbnail;

	@ManyToMany
	@JoinTable(
			name = "book_image",
			joinColumns = @JoinColumn(name = "book_id"),
			inverseJoinColumns = @JoinColumn(name = "image_id")
	) @ToString.Exclude
	private List<ImageEntity> images = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "position_id")
	private PositionEntity position;

}