package com.ictcampus.lab.base.service.book.mapper;

import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import com.ictcampus.lab.base.service.book.model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper
public interface BookServiceStructMapper {
	List<Book> toBooks( final List<BookEntity> bookEntities );

	Book toBook( final BookEntity bookEntity );
}
